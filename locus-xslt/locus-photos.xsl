<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html lang="en">
            <head>
                <meta charset="UTF-8"></meta>
                <title>Locus | Главная</title>
            </head>
            <body>
                <div style="width: 70%; margin: auto;" align="center">
                    <div align="center" style="border: 1px gray solid;">
                        <h1 align="center">Locus</h1>
                    </div>
                    <p align="left">Total value of likes: <xsl:value-of select="sum(//place/@likes)"/></p>
                    <p align="left">Total number of places: <xsl:value-of select="count(//places/place)"/></p>
                    <xsl:apply-templates select="//place">
                        <xsl:sort select="@likes" order="descending"/>
                    </xsl:apply-templates>
                    <div align="center" style="border: 1px gray solid;">
                        <span>(c)Locus 2018</span>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="place">
        <div style="border: 1px gray solid; margin-top: 20px; margin-bottom: 20px;">
            <p><h1><xsl:value-of select="@name"/></h1></p>
            <img width="300px" height="300px" src="img/{@name}.jpg" style="border: 1px gray solid; border-radius: 10px;"></img>
            <p>Description</p>
            <p><xsl:value-of select="@description"/></p>
            <p align="left">Likes:<b><xsl:value-of select="@likes"/></b></p>
        </div>
    </xsl:template>
</xsl:stylesheet>